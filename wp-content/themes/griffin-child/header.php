<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120517612-1"></script>
			<script>
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());

				gtag('config', 'UA-120517612-1');
			</script>
    <title><?php wp_title('|', true, 'right'); ?></title>
    <meta charset="<?php bloginfo('charset'); ?>" />           
    <meta name="viewport" content="width=device-width, initial-scale=1">   
    <link rel="profile" href="http://gmpg.org/xfn/11" />        
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header style="background-image: url('<?php header_image(); ?>');">
    <div id="header-content">
        <a id="hamburger" href="#"><span></span><span></span><span></span></a>    
        <?php if (get_logo_url()): ?>
            <a id="header-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                <img src='<?php echo esc_url(get_logo_url()); ?>' alt='<?php echo esc_attr(get_bloginfo('name', 'display')); ?>'>
                <span>Te prestamos B./500,00 en 30' *</span> 
            </a>
            <a href="http://pa.clicredit.com">
                <button id="header-button">¡SOLICITA AHORA!</button>
            </a>
        <?php else: ?>
            <a id="header-name" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
        <?php endif; ?> 
        <?php $header_img = get_header_image(); ?>    
    </div>
    <div id="header-social">
        <span>SÍGUENOS:</span>
        <a href="https://www.youtube.com/channel/UCIN-ceY8ydWRyHGopX3pYnw" target="_blank"><i class="fab fa-youtube"></i></a>
        <a href="https://www.instagram.com/clicredit.pa/" target="_blank"><i class="fab fa-instagram"></i></a>
        <a href="https://www.facebook.com/CliCredit.pa/" target="_blank"><i class="fab fa-facebook-square"></i></a> 
    </div>
    <?php
        $location = 'primary';
        if (has_nav_menu($location)) :
    ?>
    <nav id="primary-menu">         
        <?php wp_nav_menu(array('theme_location' => 'primary','depth' => 2,'container' => false)); ?>                    
    </nav>
    <?php endif; ?>  
</header>
   
<div class="container">
