<?php

function get_logo_url() {
  if(get_theme_mod('griffin_logo_setting')) {
    return get_theme_mod('griffin_logo_setting');
  } else {
    return '/blog/wp-content/themes/griffin-child/assets/img/clicredit-logo.svg';
  }
}

?>